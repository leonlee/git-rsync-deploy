# git-rsync-deploy

Simple deployment script using GIT and RSYNC


## Using git-rsync-deploy

Place files in git repo root and configure '.git-rsync-config':

    - DEPLOY_SOURCE_ROOT_DIR=/path/where/deploy/source/will/be/created
    - DEPLOY_DEST_DIR=/path/on/server
    - DEPLOY_GIT_BRANCH=master
    - DEPLOY_SERVER=example.com
    - DEPLOY_ACCOUNT=username
    - DEPLOY_KEY=/optional/path/to/ssh/key

Configure rsync filters in '.git-rsync-filter'

cd to git root and run `git-sync-deploy` and it will:

    - Clone/Pull git repo branch to the DEPLOY_SOURCE_ROOT_DIR/GIT_REPO_NAME/DEPLOY_GIT_BRANCH/
    - Reset and clean copied git repo
    - Upload this repo source to destination


## Todo

    - Different stages, eg. staging, deploy, etc., to upload to diferent destinations
    - Allow to define and run some build scripts before uploading to destination
