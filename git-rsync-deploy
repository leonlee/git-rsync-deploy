#!/bin/bash

set -e

v=0.0.2

git_branch() {
  br=`git branch | grep "*"`
  echo ${br/* /}
}


# help
if [ "$1" = "-h" ] || [ "$1" = "--help" ] ; then
    echo ""
    echo "git-rsync-deploy $v"
    echo "Simple deployment script using GIT and RSYNC"
    echo ""
    echo "git-rsync-deploy -option"
    echo "  -h, --help Show this help"
    echo "  -v, --version Show version"
    echo "  -t, --test Run in test mode with rsync option --dry-run"
    exit 0
fi


# version
if [ "$1" = "-v" ] || [ "$1" = "--version" ] ; then
    echo "$v"
    exit 0
fi


# Test mode with rsync --dry-run
DRY_RUN=""
if [ "$1" = "-t" ] || [ "$1" = "--test" ] ; then
    echo "Switching to test mode (rsync --dry-run)"
    DRY_RUN=--dry-run
fi

GIT_BRANCH="$(git_branch)"


while true
do
  echo "[1] Development"
  echo "[2] Production"
  read -p "Choose the enviroment: " answer
  # (2) handle the input we were given
  case $answer in
   [1]* ) CONFIG_FILE=".git-rsync-config-development"
          echo "Set enviroment is 'Development', config file is '$CONFIG_FILE'"
           break;;

   [2]* ) CONFIG_FILE=".git-rsync-config"
          echo "Set enviroment is 'Production', config file is '$CONFIG_FILE'"
           break;;

   * )     echo "Dude, just enter 1 or 2";;
  esac
done


# check for the config file in current folder
DIR="$(pwd)"
if [ ! -e $DIR/$CONFIG_FILE ] ; then
    echo "Can not find '$CONFIG_FILE' in current folder '$DIR'"
    echo "Check that you are in projects root folder"
    exit 1
else
    # Set the config by loading from the config file in the same directory
    source $DIR/$CONFIG_FILE
    # set git repo
    export DEPLOY_GIT_REPO=$DIR
fi


# construct deploy source folder, where git repo will be cloned to and prepared for rsync
version="$(git describe --always --dirty)"
repo_name=${PWD##*/}
export DEPLOY_SOURCE_DIR="$DEPLOY_SOURCE_ROOT_DIR/$repo_name/$DEPLOY_GIT_BRANCH/"
#DEPLOY_SOURCE_DIR="$DEPLOY_SOURCE_ROOT_DIR/$repo_name/$DEPLOY_GIT_BRANCH_$version"


# Show config
echo ""
echo "Deploy config:"
echo "DEPLOY_SOURCE_ROOT_DIR $DEPLOY_SOURCE_ROOT_DIR"
echo "DEPLOY_SOURCE_DIR $DEPLOY_SOURCE_DIR"
echo "DEPLOY_GIT_REPO $DEPLOY_GIT_REPO"
echo "DEPLOY_GIT_BRANCH $DEPLOY_GIT_BRANCH"
echo "DEPLOY_DEST_DIR $DEPLOY_DEST_DIR"
echo "DEPLOY_SERVER $DEPLOY_SERVER"
echo "DEPLOY_SERVER_PORT $DEPLOY_SERVER_PORT"
echo "DEPLOY_ACCOUNT $DEPLOY_ACCOUNT"
echo "DEPLOY_KEY $DEPLOY_KEY"
echo "DEPLOY_SCRIPT $DEPLOY_SCRIPT"
echo ""


# prompt for confirmation
read -p "Are you sure [Y/n]? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY != "Y" ]] ; then
    echo "Deploy canceled"
    exit 1
fi


# clone to deploy folder
if [ ! -e $DEPLOY_SOURCE_DIR ] ; then
    git_op="-b $DEPLOY_GIT_BRANCH"
    git_msg=" from branch $DEPLOY_GIT_BRANCH"
    echo "making git repo in $DEPLOY_SOURCE_DIR$git_msg"
    #mkdir -p "$(basename $repo_location)"
    mkdir -p "$DEPLOY_SOURCE_DIR"
    echo git clone $git_op $DEPLOY_GIT_REPO $DEPLOY_SOURCE_DIR
    git clone $git_op $DEPLOY_GIT_REPO $DEPLOY_SOURCE_DIR
else
    # pull from master to ensure we're uptodate
    (
        cd $DEPLOY_SOURCE_DIR
        #git_op="$DEPLOY_GIT_BRANCH:$DEPLOY_GIT_BRANCH"
        #echo cd $DEPLOY_SOURCE_DIR "&&" git pull origin $git_op
        #git pull origin $git_op
        git_op="origin/$DEPLOY_GIT_BRANCH"
        echo cd $DEPLOY_SOURCE_DIR "&&" git fetch origin "&&" git reset --hard $git_op "&&" git clean -df
        git fetch origin
        git reset --hard $git_op
        git clean -df
        )
fi


# write deploy id
deploy_date=$(date +"%Y-%m-%d-%s")
delimiter="_version-"
version_id="$deploy_date$delimiter$version"
log="_error.log"
echo "v$version deployed on $deploy_date" > $DEPLOY_SOURCE_DIR$version_id


# run deploy script
if [ ! -e $DEPLOY_SCRIPT ]; then
   # no deploy script found
   echo "No deploy script defined"
else
   # execute script
   echo "Executing deploy script '$DEPLOY_SCRIPT'"
   $DEPLOY_SCRIPT
fi

# start rsync
echo "Starting rsync:"
if [ ! -e $DEPLOY_KEY ]; then
   # access by tediously typing a password over and again
   rsync $DRY_RUN -L --delete --chmod=ug=rwX --perms --omit-dir-times -e "ssh -p $DEPLOY_SERVER_PORT" -axv --progress --filter="merge $DIR/.git-rsync-filter" \
   $DEPLOY_SOURCE_DIR $DEPLOY_ACCOUNT@$DEPLOY_SERVER:$DEPLOY_DEST_DIR
else
   # access by key
   rsync $DRY_RUN -L --delete --chmod=ug=rwX --perms --omit-dir-times -axv --progress --filter="merge $DIR/.git-rsync-filter" \
   -e "ssh -i $DEPLOY_KEY -p $DEPLOY_SERVER_PORT" \
   $DEPLOY_SOURCE_DIR $DEPLOY_ACCOUNT@$DEPLOY_SERVER:$DEPLOY_DEST_DIR
fi
